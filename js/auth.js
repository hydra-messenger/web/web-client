$(document).ready(function() {
  let recaptcha_response = false;
  $("#iuth-id")[0].oninput = function() {
    if ($("#iuth-id").val().length == 0) {
      $("#auth-button")[0].innerText = "Sign up";
      $("#auth-button")[0].style = "background-color: #28a745; border-color: #28a745;";
    }
    else {
      $("#auth-button")[0].innerText = "Sign in";
      $("#auth-button")[0].style = "background-color: #5682a3; border-color: #5682a3;";
    }
  };

  function authGo(response_token) {
    console.info(anime);
    $.post("https://api.hydra.ws/auth", {"isRobot": response_token, })
  }

  textmore.onclick = function() {
    if (textmore.innerText == "Learn more") {
      $("#auth_form_s")[0].classList.add("hidden_s");
      $("#auth_form_ss")[0].classList.remove("hidden_s");
      textmore.innerText = "Back to login";
    }
    else {
      $("#auth_form_ss")[0].classList.add("hidden_s");
      $("#auth_form_s")[0].classList.remove("hidden_s");
      textmore.innerText = "Learn more";
    }
  };
});
